export const environment = {
  production: true,
  firebase: {
    apiKey: "AIzaSyBcIS8UYrrBhph5a4VaRq7KI-0bCdA6C28",
    authDomain: "scottmcc-ui.firebaseapp.com",
    databaseURL: "https://scottmcc-ui.firebaseio.com",
    projectId: "scottmcc-ui",
    storageBucket: "scottmcc-ui.appspot.com",
    messagingSenderId: "901127633830",
    appId: "1:901127633830:web:ada6660368f85b828dad67",
    measurementId: "G-27PTVW9026"
  }
};
