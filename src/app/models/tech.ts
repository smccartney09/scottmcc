export interface Tech {
  name: string;
  logoUrl: string;
  priority?: number;
}
