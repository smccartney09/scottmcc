import { DocumentReference } from '@angular/fire/firestore';

export interface Project {
  name: string;
  description: string;
  techs: DocumentReference[];
  hidden: boolean;
  websiteUrl?: string;
  gitUrl?: string;
  docUrl?: string;
  priority?: number;
}
