import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';

import { TechChipComponent } from './tech-chip.component';

describe('TechChipComponent', () => {
  let component: TechChipComponent;
  let fixture: ComponentFixture<TechChipComponent>;

  beforeEach(waitForAsync(() => {
    TestBed.configureTestingModule({
      declarations: [ TechChipComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TechChipComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
