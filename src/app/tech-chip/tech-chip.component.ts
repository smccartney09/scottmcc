import { Component, OnInit, Input } from '@angular/core';
import { Observable } from 'rxjs';
import { TechService } from 'src/app/services/tech.service';
import { Tech } from 'src/app/models/tech';

@Component({
  selector: 'app-tech-chip',
  templateUrl: './tech-chip.component.html',
  styleUrls: ['./tech-chip.component.scss']
})
export class TechChipComponent implements OnInit {

  @Input() tech: Tech;
  logo$: Observable<string>;

  constructor(private techService: TechService) { }

  ngOnInit() {
    this.logo$ = this.techService.loadImg(this.tech.logoUrl);
  }

}
