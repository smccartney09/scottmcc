import { Component, OnInit, Input } from '@angular/core';
import { TechService } from 'src/app/services/tech.service';
import { Project } from 'src/app/models/project';
import { Tech } from 'src/app/models/tech';

@Component({
  selector: 'app-project',
  templateUrl: './project.component.html',
  styleUrls: ['./project.component.scss']
})
export class ProjectComponent implements OnInit {

  @Input() project: Project;
  techs: Tech[] = [];

  constructor(private techService: TechService) { }

  ngOnInit() {
    this.project.techs.forEach(ref => this.loadTech(ref));
  }

  private async loadTech(techRef: any): Promise<void> {
    const tech = await this.techService.loadTechReference(techRef);
    this.techs.push(tech);
  }
}
