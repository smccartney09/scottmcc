import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { MaterialModule } from './material.module';
import { FirebaseModule } from './firebase.module';

import { AppComponent } from './app.component';
import { TechStackComponent } from './tech-stack/tech-stack.component';
import { ProjectComponent } from './project/project.component';
import { TechService } from 'src/app/services/tech.service';
import { ProjectService } from 'src/app/services/project.service';
import { ResumeService } from 'src/app/services/resume.service';
import { TechChipComponent } from './tech-chip/tech-chip.component';
import { BioComponent } from './bio/bio.component';

@NgModule({
  declarations: [
    AppComponent,
    TechStackComponent,
    ProjectComponent,
    TechChipComponent,
    BioComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    BrowserAnimationsModule,
    HttpClientModule,
    AppRoutingModule,
    MaterialModule,
    FirebaseModule
  ],
  providers: [TechService, ProjectService, ResumeService],
  bootstrap: [AppComponent]
})
export class AppModule { }
