import { Component } from '@angular/core';
import { Title, Meta } from '@angular/platform-browser';

import { ProjectService } from 'src/app/services/project.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  phoneNum = '(636) 236-3458';
  email = 'contact@scottmcc.com';
  showPhone = false;
  showEmail = false;
  pageMetadata = {
    title: 'Scott McCartney - Full Stack Software Engineer',
    description:
      'A full stack software engineer creating collaborative online tools with professional experience in Angular, SpringBoot, and Firebase.',
    image: 'https://i.ibb.co/3YjFQSC/scottmcc-avatar.jpg',
  };

  constructor(
    public projectService: ProjectService,
    private title: Title,
    private meta: Meta
  ) {
    this.title.setTitle(this.pageMetadata.title);
    this.meta.addTags([
      { name: 'description', content: this.pageMetadata.description },
      { name: 'twitter:card', content: 'summary' },
      { name: 'twitter:creator', content: 'skittlesMc9' },
      { name: 'og:url', content: 'https://scottmcc.com' },
      { name: 'og:title', content: this.pageMetadata.title },
      { name: 'og:description', content: this.pageMetadata.description },
      { name: 'og:image', content: this.pageMetadata.image },
    ]);
  }

  togglePhone(): void {
    this.showEmail = false;
    this.showPhone = !this.showPhone;
  }

  toggleEmail(): void {
    this.showPhone = false;
    this.showEmail = !this.showEmail;
  }
}
