import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AngularFirestore, DocumentReference } from '@angular/fire/firestore';
import { AngularFireStorage } from '@angular/fire/storage';
import { Observable } from 'rxjs';
import { Tech } from 'src/app/models/tech';

@Injectable({
  providedIn: 'root'
})
export class TechService {

  public techs$: Observable<Tech[]>;

  constructor(
    private firestore: AngularFirestore,
    private storage: AngularFireStorage,
    @Inject(PLATFORM_ID) private platform: any
  ) {
    if (isPlatformBrowser(this.platform)) {
      this.loadTechs();
    }
  }

  public loadTechs(): void {
    this.techs$ = this.firestore
      .collection<Tech>('techs', ref => ref.orderBy('priority'))
      .valueChanges();
  }

  public loadImg(storagePath: string): Observable<string> {
    return this.storage.ref(storagePath).getDownloadURL();
  }

  public async loadTechReference(ref: DocumentReference): Promise<Tech> {
    return new Promise<Tech>((resolve, reject) => {
      this.firestore.doc<Tech>(ref.path)
        .valueChanges()
        .subscribe(data => {
          if (data) { resolve(data); }
          else { reject('Unable to find tech document.'); }
      });
    });
  }
}
