import { Injectable } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ResumeService {

  constructor(private afStorage: AngularFireStorage,
              private http: HttpClient) { }

  public download(): void {
    this.afStorage.ref('files/Resume.pdf')
                  .getDownloadURL()
                  .subscribe(url => this.retrieveByUrl(url));
  }

  private retrieveByUrl(url: string): void {
    this.http.get(url, { responseType: 'blob' })
             .subscribe(data => this.downloadToBrowser(data));
  }

  private downloadToBrowser(contents: Blob): void {
      const downloadURL = window.URL.createObjectURL(contents);
      const link = document.createElement('a');
      link.href = downloadURL;
      link.download = "McCartney_Resume.pdf";
      link.click();
  }
}
