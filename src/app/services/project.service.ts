import { Injectable, Inject, PLATFORM_ID } from '@angular/core';
import { isPlatformBrowser } from '@angular/common';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Project } from 'src/app/models/project';

@Injectable({
  providedIn: 'root'
})
export class ProjectService {

  public projects$: Observable<Project[]>;

  constructor(
    private firestore: AngularFirestore,
    @Inject(PLATFORM_ID) private platform: any
  ) {
    if (isPlatformBrowser(this.platform)) {
      this.loadProjects();
    }
  }

  private loadProjects(): void {
    this.projects$ = this.firestore
      .collection<Project>('projects', ref =>
        ref.where('hidden', '==', false)
           .orderBy('priority'))
      .valueChanges();
  }
}
