import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFireStorage } from '@angular/fire/storage';
import { AngularFireAnalytics } from '@angular/fire/analytics';

import { ResumeService } from 'src/app/services/resume.service';

export interface BioDetail {
  identifier: string;
  content: string;
}

@Component({
  selector: 'app-bio',
  templateUrl: './bio.component.html',
  styleUrls: ['./bio.component.scss'],
})
export class BioComponent implements OnInit {
  avatar$: Observable<string>;
  details: BioDetail[] = [
    {
      identifier: 'location',
      content: 'cincinnati, oh (or remote)',
    },
    {
      identifier: 'experience',
      content: `
      3+ years at fortune 100 company building stable and extensible backend
      systems and accessible UI's that were used by customers nationwide
      `,
    },
    {
      identifier: 'interested in',
      content: `
      opportunities to be a technical lead for a SaaS startup
      `,
    },
  ];

  constructor(
    private resumeService: ResumeService,
    private storage: AngularFireStorage,
    private analytics: AngularFireAnalytics
  ) {
    this.avatar$ = this.storage.ref('avatar/avatar.jpg').getDownloadURL();
  }

  ngOnInit(): void {}

  downloadResume(): void {
    this.analytics.logEvent('resumeDownload');
    this.resumeService.download();
  }
}
