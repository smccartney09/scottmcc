import { Component, OnInit } from '@angular/core';
import { TechService } from 'src/app/services/tech.service';

@Component({
  selector: 'app-tech-stack',
  templateUrl: './tech-stack.component.html',
  styleUrls: ['./tech-stack.component.scss']
})
export class TechStackComponent implements OnInit {

  constructor(public techService: TechService) { }

  ngOnInit() {
  }

}
